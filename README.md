# django-filter demo project

Demo project to clarify my StackOverflow question ["django-filter: How to filter by field A but display field B as corresponding filter option/text?"](https://stackoverflow.com/q/44854719/5071435)


## Running Locally

```bash
git clone https://gitlab.com/tombreit/django-filter-demo.git
cd django-filter-demo
pyvenv venv # setup python3 virtual environment
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```

* The django admin with some data is accessible at

  http://127.0.0.1:8000/admin/

  User: admin // PW: 1

* The demo django filter site is accessible at

  http://127.0.0.1:8000/blog/
