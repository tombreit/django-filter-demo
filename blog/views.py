from django.shortcuts import render
from pprint import pprint
from .filters import PostFilter
from .models import Post

def index(request):
    post_list = Post.objects.all()
    post_filter = PostFilter(request.GET, queryset=post_list)

    #print(dir(post_filter))
    #pprint(vars(post_filter))
    
    return render(request, 'blog/index.html', {'filter': post_filter})
