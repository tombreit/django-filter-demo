import django_filters
from django_filters.widgets import LinkWidget
from .models import Post


class PostFilter(django_filters.FilterSet):
    categories = django_filters.AllValuesFilter(
        name="categories__slug",
        label="Categories",
        widget=LinkWidget(),
    )

    class Meta:
        model = Post
        fields = ['categories',]
